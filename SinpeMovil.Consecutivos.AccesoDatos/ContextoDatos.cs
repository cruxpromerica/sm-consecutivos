﻿using Comunes.EF6.Repositorios;
using System.Transactions;
using System.Data.Entity;

namespace SinpeMovil.Consecutivos.AccesoDatos
{
	
	/// <summary>
	/// En esta clase se debe configurar una propiedad dbSet por cada una de las tablas de la BD relacionadas al servicio
	/// en cuestion.
	/// </summary>
	public class ContextoDatos : ContextoDatosBase
	{

		private static TransactionOptions? mOpcionTransaccion = null;

		#region "Constructor"
		/// <summary>
		/// Se indica el nombre de la entrada del string de conexion existente en el .config o en el machine.config
		/// </summary>
		/// <param name="nombreDelStringDeConexion"></param>
		/// <remarks></remarks>
		public ContextoDatos(string nombreDelStringDeConexion)
			: base(nombreDelStringDeConexion)
		{
		}

		public ContextoDatos()
			: base("SINPE_MOVIL")
		{
		}
		#endregion

		/// En este metodo se pueden sobre escribir algunos parametros del repositorio en particualar
		/// Por ej. si se tiene una tabla con un nombre distinto al del modelo Si.Datos se debe de utlizar la extension
		/// modelbuilder.MapearEntidadBD(Modelo))
		/// Donde el Datos.Modelo debe de tener el ConfiguracionBDAttribute indicando el nombre fisico de la tabla en BD  
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}

		public static TransactionOptions? TransaccionIsolationLevel
		{
			get
			{
				// TODO: Cargar de configuracion
				if (!mOpcionTransaccion.HasValue)
					mOpcionTransaccion = new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.Snapshot };

				return mOpcionTransaccion;
			}
		}
	}
}