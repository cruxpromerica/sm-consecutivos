﻿using Comunes;
using Comunes.Auditoria;
using Comunes.Auditoria.Extensiones.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Runtime;

namespace SinpeMovil.Consecutivos.Servicios
{
	public class InitializeService
	{
		public static void AppInitialize()
		{
			InjectionConstructor firma = new InjectionConstructor(new object[] { });

			ContenedorUnity.Instancia.RegisterTypes(
				UnityHelpers.GetTypesWithCustomAttribute<UnityAutoRegistrationAttribute>(AllClasses.FromLoadedAssemblies()),
				i => WithMappings.FromMatchingInterface(i),
				i => WithName.Default(i),
				i => WithLifetime.Transient(i)
			);
			
			var mappingsBaseException = new NameValueCollection()
			{
				{"IdManejo", "{Guid}"}
			};

			var bitacoraHandler = new BitacoraExceptionHandler("Negocio",
																100,
																TraceEventType.Error,
																"Enterprise Library Exception Handling",
																(int)PrioridadEvento.Media,
																typeof(Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter),
																null,
																true);

			var exceptionShieldingPolicy = new System.Collections.Generic.List<ExceptionPolicyEntry>()
			{
				new ExceptionPolicyEntry(typeof(System.Exception),
					PostHandlingAction.ThrowNewException,
					new IExceptionHandler[] {
						bitacoraHandler,
						new FaultContractExceptionHandler(typeof(DetalladaFault),
							"Ocurrió un error en el procesamiento de la operación.",
							mappingsBaseException)
					})
			};

			var bitacoraPolicy = new List<ExceptionPolicyEntry>()
			{
				new ExceptionPolicyEntry(typeof(Exception),
					PostHandlingAction.NotifyRethrow,
					new IExceptionHandler[] { bitacoraHandler })
			};

			var policies = new List<ExceptionPolicyDefinition>()
			{
				new ExceptionPolicyDefinition("WCF Exception Shielding", exceptionShieldingPolicy),
				new ExceptionPolicyDefinition("Bitacora", bitacoraPolicy)
			};

			var exManager = new ExceptionManager(policies);
			ExceptionPolicy.Reset();
			ExceptionPolicy.SetExceptionManager(exManager);
		}

	}
}