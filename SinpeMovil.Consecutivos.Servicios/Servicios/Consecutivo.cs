﻿using SinpeMovil.Consecutivos.Contratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Microsoft.Practices.Unity;

namespace SinpeMovil.Consecutivos.Servicios
{

	public class Consecutivo : IConsecutivo
	{
		public string SinpeMovilObtenerCodigoDeReferenciaUnico()
		{
			var caracteristica = ContenedorUnity.Instancia.Resolve<Dominio.Caracteristicas.ObtenerCodigoDeReferenciaSinpeMovil>();
			return caracteristica.GenerarCodReferenciaUnico();
		}
	}
}
