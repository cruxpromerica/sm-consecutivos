﻿using Comunes.LogicaNegocio.Dominio.Acciones;
using SinpeMovil.Consecutivos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace SinpeMovil.Consecutivos.Dominio.Caracteristicas
{
	public sealed class ObtenerCodigoDeReferenciaSinpeMovil
	{
		public string GenerarCodReferenciaUnico()
		{
			var accion = ContenedorUnity.Instancia.Resolve<Dominio.Acciones.Consecutivos>();
			
			string codReferencia = null;
			long consecutivo = accion.ObtenerConsecutivoCodigoReferencia();
			codReferencia = DateTime.Now.ToString("yyyyMMdd") + Constantes.CodEntidadPromerica + Constantes.CodServicioSinpeMovil + String.Format("{0:D9}", consecutivo) + "00";
			codReferencia = codReferencia + DigitoVerificador.CalcularDigitoVerificador(codReferencia);
			return codReferencia;
		}
	}
}
