﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using SinpeMovil.Consecutivos.AccesoDatos;

namespace SinpeMovil.Consecutivos.Dominio.Acciones
{
	public class Consecutivos
	{
		internal long ObtenerConsecutivoCodigoReferencia()
		{
			long consecutivo = 0;
			using (var contextoDatos = new ContextoDatos())
			{
				consecutivo = (long)contextoDatos.EjecutarSqlCommandText<long>("SELECT NEXT VALUE FOR ConsecutivoCodigoReferencia");
			}
			return consecutivo;
		}

	}
}
