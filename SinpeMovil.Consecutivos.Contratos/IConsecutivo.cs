﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Consecutivos.Contratos
{
	[ServiceContract()]
	public interface IConsecutivo
	{
		[OperationContract]
		[FaultContract(typeof(DetalladaFault))]
		string SinpeMovilObtenerCodigoDeReferenciaUnico();
	}
}
